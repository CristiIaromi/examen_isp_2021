import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

public class S2 {
    public static void main(String[] args) {
        Window.getInstance();
    }
}

class Window extends JFrame {
    private static Window instance;
    private JButton run;
    private JTextArea textArea;

    public Window() {
        this.setTitle("Exam");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(2, 1));
        this.addComponents();
        this.setBounds(300, 300, 200, 230);
        this.setVisible(true);
    }

    private void addComponents() {
        this.textArea = new JTextArea();
        JScrollPane scroll = new JScrollPane(this.textArea);
        this.add(scroll);

        this.run = new JButton("Run");
        this.run.setFont(new Font(null, Font.PLAIN, 18));
        this.run.addActionListener(new ButtonActionListener());
        this.add(this.run);
    }

    public JTextArea getTextArea() {
        return this.textArea;
    }

    public static Window getInstance() {
        if (Window.instance == null) {
            Window.instance = new Window();
        }
        return Window.instance;
    }
}

class ButtonActionListener implements ActionListener {
    private static final String PATH = "src/main/resources/data.txt";

    @Override
    public void actionPerformed(ActionEvent event) {
        try (FileWriter out = new FileWriter(PATH)) {
            String text = Window.getInstance().getTextArea().getText();
            out.write(text);
            Window.getInstance().getTextArea().setText("");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}